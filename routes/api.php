<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth.apikey'], function () {
    Route::prefix('customers')->group(function () {
        Route::post('showAll', 'CustomerController@showAll');
        Route::post('show', 'CustomerController@showWithId');
        Route::post('create', 'CustomerController@create');
        Route::put('update', 'CustomerController@update');
        Route::delete('delete', 'CustomerController@delete');
    });

    Route::prefix('products')->group(function () {
        Route::post('showAll', 'ProductController@showAll');
        Route::post('show', 'ProductController@showWithId');
        Route::post('create', 'ProductController@create');
        Route::put('update', 'ProductController@update');
        Route::delete('delete', 'ProductController@delete');
    });

    Route::prefix('orders')->group(function () {
        Route::post('showAll', 'OrderController@showAll');
        Route::post('show', 'OrderController@showWithId');
        Route::post('create', 'OrderController@create');
        Route::put('update', 'OrderController@update');
        Route::delete('delete', 'OrderController@delete');
    });

    Route::prefix('transactions')->group(function () {
        Route::post('showAll', 'TransactionController@showAll');
        Route::post('show', 'TransactionController@showWithId');
        Route::post('create', 'TransactionController@create');
        Route::put('update', 'TransactionController@update');
        Route::delete('delete', 'TransactionController@delete');
    });
});
