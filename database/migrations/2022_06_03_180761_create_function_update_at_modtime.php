<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateFunctionUpdateAtModtime extends Migration
{
    // CREATE FUNCTION FOR UPDATE updated_at AUTOMATICALLY

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP FUNCTION IF EXISTS updated_at_modtime()");

        DB::statement("
            CREATE FUNCTION updated_at_modtime() RETURNS trigger
                LANGUAGE plpgsql
                AS $$
              BEGIN
                NEW.updated_at = NOW();
                RETURN NEW;
              END;
            $$;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP FUNCTION IF EXISTS updated_at_modtime()");
    }
}
