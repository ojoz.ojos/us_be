<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['order_id' => 1, 'product_id' => 1, 'quantity' => 2],
            ['order_id' => 1, 'product_id' => 2, 'quantity' => 2],
            ['order_id' => 2, 'product_id' => 6, 'quantity' => 1],
            ['order_id' => 3, 'product_id' => 4, 'quantity' => 1],
            ['order_id' => 3, 'product_id' => 7, 'quantity' => 1],
            ['order_id' => 3, 'product_id' => 6, 'quantity' => 1],
            ['order_id' => 4, 'product_id' => 9, 'quantity' => 2],
            ['order_id' => 4, 'product_id' => 10, 'quantity' => 2],
            ['order_id' => 5, 'product_id' => 3, 'quantity' => 1],
            ['order_id' => 6, 'product_id' => 7, 'quantity' => 1],
            ['order_id' => 6, 'product_id' => 5, 'quantity' => 1],
            ['order_id' => 6, 'product_id' => 3, 'quantity' => 1],
            ['order_id' => 7, 'product_id' => 5, 'quantity' => 1],
            ['order_id' => 7, 'product_id' => 6, 'quantity' => 1],
            ['order_id' => 7, 'product_id' => 7, 'quantity' => 1],
            ['order_id' => 7, 'product_id' => 8, 'quantity' => 1],
            ['order_id' => 8, 'product_id' => 5, 'quantity' => 1],
            ['order_id' => 8, 'product_id' => 9, 'quantity' => 1],
            ['order_id' => 9, 'product_id' => 5, 'quantity' => 1]
        ];

        DB::table('order_products')->insert($data);
    }
}
