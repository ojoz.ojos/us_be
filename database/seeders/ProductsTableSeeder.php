<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'PEN', 'category' => 'ATK', 'price' => 15000],
            ['name' => 'PENSIL', 'category' => 'ATK', 'price' => 10000],
            ['name' => 'PAYUNG', 'category' => 'RT', 'price' => 70000],
            ['name' => 'PANCI', 'category' => 'MASAK', 'price' => 110000],
            ['name' => 'SAPU', 'category' => 'RT', 'price' => 40000],
            ['name' => 'KIPAS', 'category' => 'ELEKTRONIK', 'price' => 200000],
            ['name' => 'KUALI', 'category' => 'MASAK', 'price' => 120000],
            ['name' => 'SIKAT', 'category' => 'RT', 'price' => 30000],
            ['name' => 'GELAS', 'category' => 'RT', 'price' => 25000],
            ['name' => 'PIRING', 'category' => 'RT', 'price' => 35000]
        ];

        DB::table('products')->insert($data);
    }
}
