<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'ANDI', 'domicile' => 'JAK-UT', 'gender' => 'PRIA'],
            ['name' => 'BUDI', 'domicile' => 'JAK-BAR', 'gender' => 'PRIA'],
            ['name' => 'JOHAN', 'domicile' => 'JAK-SEL', 'gender' => 'PRIA'],
            ['name' => 'SINTHA', 'domicile' => 'JAK-TIM', 'gender' => 'WANITA'],
            ['name' => 'ANTO', 'domicile' => 'JAK-UT', 'gender' => 'PRIA'],
            ['name' => 'BUJANG', 'domicile' => 'JAK-BAR', 'gender' => 'PRIA'],
            ['name' => 'JOWAN', 'domicile' => 'JAK-SEL', 'gender' => 'PRIA'],
            ['name' => 'SINTIA', 'domicile' => 'JAK-TIM', 'gender' => 'WANITA'],
            ['name' => 'BUTET', 'domicile' => 'JAK-BAR', 'gender' => 'WANITA'],
            ['name' => 'JONNY', 'domicile' => 'JAK-SEL', 'gender' => 'WANITA']
        ];

        DB::table('customers')->insert($data);
    }
}
