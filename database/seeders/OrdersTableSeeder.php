<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['date' => '2018-01-01', 'customer_id' => 1, 'subtotal' => 50000],
            ['date' => '2018-01-01', 'customer_id' => 2, 'subtotal' => 200000],
            ['date' => '2018-01-01', 'customer_id' => 3, 'subtotal' => 430000],
            ['date' => '2018-01-02', 'customer_id' => 7, 'subtotal' => 120000],
            ['date' => '2018-01-02', 'customer_id' => 4, 'subtotal' => 70000],
            ['date' => '2018-01-03', 'customer_id' => 8, 'subtotal' => 230000],
            ['date' => '2018-01-03', 'customer_id' => 9, 'subtotal' => 390000],
            ['date' => '2018-01-03', 'customer_id' => 5, 'subtotal' => 65000],
            ['date' => '2018-01-04', 'customer_id' => 2, 'subtotal' => 40000]
        ];

        DB::table('orders')->insert($data);
    }
}
