<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Order
{
    const logPath = "order";

    public function showAll($request): array
    {
        $limit = $request->limit;

        return DB::select("
            SELECT *
            FROM orders
            LIMIT $limit
        ");
    }

    public function showWithId($request): array
    {
        $orderId = $request->id;

        return DB::select("
            SELECT *
            FROM orders
            WHERE id = $orderId
        ");
    }

    public function create($request)
    {
        DB::beginTransaction();

        try {
            $date = $request->date;
            $customer_id = $request->customer_id;
            $subtotal = $request->subtotal;

            $query = "
                INSERT INTO orders (date, customer_id, subtotal)
                VALUES (:date, :customer_id, :subtotal)
            ";
            $params = ['date' => $date, 'customer_id' => $customer_id, 'subtotal' => $subtotal];

            DB::insert($query, $params);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "CreateOrder | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }

    public function update($request)
    {
        DB::beginTransaction();

        try {
            $orderId = $request->id;
            $dataRequest = $request->all();
            unset($dataRequest['id']);
            $arrayKeysRequest = array_keys($dataRequest);

            if (empty($arrayKeysRequest)) {
                return 0;
            }

            $vCol = array_map(function ($item) {
                return "$item = :$item";
            }, $arrayKeysRequest);
            $vCol = implode(', ', $vCol);

            $query = "
                UPDATE orders
                SET $vCol
                WHERE id = $orderId
            ";
            $params = [];
            foreach ($arrayKeysRequest as $item) {
                $params[$item] = $dataRequest[$item];
            }

            DB::update($query, $params);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "UpdateOrder | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }

    public function delete($request)
    {
        DB::beginTransaction();

        try {
            $orderId = $request->id;

            $query = "
                DELETE FROM orders
                WHERE id = $orderId
            ";
            DB::update($query);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "DeleteOrder | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }
}
