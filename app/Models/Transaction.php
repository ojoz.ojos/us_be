<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Transaction
{
    const logPath = "transaction";

    public function showAll($request)
    {
        $limit = $request->limit;

        $data = DB::select("
            SELECT
                o.id AS order_id, o.date AS order_date, o.subtotal AS order_subtotal,
                c.name AS customer_name,
                op.quantity AS op_quantity,
                p.name AS product_name, p.category AS product_category, p.price AS product_price
            FROM orders o
                JOIN customers c ON o.customer_id = c.id
                JOIN order_products op ON o.id = op.order_id
                JOIN products p ON p.id = op.product_id
            ORDER BY order_id DESC
            LIMIT $limit
        ");

        return collect($data)->groupBy('order_id', false)->values()->map(function ($order) {
            return [
                'id' => $order[0]->order_id,
                'date' => $order[0]->order_date,
                'subtotal' => $order[0]->order_subtotal,
                'customer' => $order->groupBy('customer_name', false)->values()->map(function ($customer) {
                    return [
                        'name' => $customer[0]->customer_name,
                    ];
                })->first(),
                'products' => $order->groupBy('product_name', false)->values()->map(function ($product) {
                    return [
                        'name' => $product[0]->product_name,
                        'category' => $product[0]->product_category,
                        'price' => $product[0]->product_price,
                        'quantity' => $product[0]->op_quantity,
                    ];
                }),
            ];
        });
    }

    public function showWithId($request): array
    {
        $orderId = $request->id;

        return DB::select("
            SELECT *
            FROM orders
            WHERE id = $orderId
        ");
    }

    public function create($request)
    {
        DB::beginTransaction();

        try {
            $date = $request->date;
            $customer_id = $request->customer;
            $products = json_decode($request->products, true);

            $query = "
                INSERT INTO orders (date, customer_id, subtotal)
                VALUES (:date, :customer_id, :subtotal)
                RETURNING id
            ";
            $params = ['date' => $date, 'customer_id' => $customer_id, 'subtotal' => 0];

            $transaction_id = DB::select($query, $params)[0]->id;

            foreach ($products as $product) {
                $query = "
                    INSERT INTO order_products (order_id, product_id, quantity)
                    VALUES (:order_id, :product_id, :quantity)
                ";
                $params = ['order_id' => $transaction_id, 'product_id' => $product['product'], 'quantity' => $product['quantity']];

                DB::insert($query, $params);
            }

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "CreateOrder | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }

    public function update($request)
    {
        DB::beginTransaction();

        try {
            $orderId = $request->id;
            $dataRequest = $request->all();
            unset($dataRequest['id']);
            $arrayKeysRequest = array_keys($dataRequest);

            if (empty($arrayKeysRequest)) {
                return 0;
            }

            $vCol = array_map(function ($item) {
                return "$item = :$item";
            }, $arrayKeysRequest);
            $vCol = implode(', ', $vCol);

            $query = "
                UPDATE orders
                SET $vCol
                WHERE id = $orderId
            ";
            $params = [];
            foreach ($arrayKeysRequest as $item) {
                $params[$item] = $dataRequest[$item];
            }

            DB::update($query, $params);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "UpdateOrder | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }

    public function delete($request)
    {
        DB::beginTransaction();

        try {
            $orderId = $request->id;

            $query = "
                DELETE FROM orders
                WHERE id = $orderId
            ";
            DB::update($query);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "DeleteOrder | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }
}
