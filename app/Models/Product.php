<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Product
{
    const logPath = "product";

    public function showAll($request): array
    {
        $limit = $request->limit;

        return DB::select("
            SELECT *
            FROM products
            LIMIT $limit
        ");
    }

    public function showWithId($request): array
    {
        $productId = $request->id;

        return DB::select("
            SELECT *
            FROM products
            WHERE id = $productId
        ");
    }

    public function create($request)
    {
        DB::beginTransaction();

        try {
            $name = $request->name;
            $category = $request->category;
            $price = $request->price;

            $query = "
                INSERT INTO products (name, category, price)
                VALUES (:name, :category, :price)
            ";
            $params = ['name' => $name, 'category' => $category, 'price' => $price];

            DB::insert($query, $params);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "CreateProduct | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }

    public function update($request)
    {
        DB::beginTransaction();

        try {
            $productId = $request->id;
            $dataRequest = $request->all();
            unset($dataRequest['id']);
            $arrayKeysRequest = array_keys($dataRequest);

            if (empty($arrayKeysRequest)) {
                return 0;
            }

            $vCol = array_map(function ($item) {
                return "$item = :$item";
            }, $arrayKeysRequest);
            $vCol = implode(', ', $vCol);

            $query = "
                UPDATE products
                SET $vCol
                WHERE id = $productId
            ";
            $params = [];
            foreach ($arrayKeysRequest as $item) {
                $params[$item] = $dataRequest[$item];
            }

            DB::update($query, $params);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "UpdateProduct | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }

    public function delete($request)
    {
        DB::beginTransaction();

        try {
            $productId = $request->id;

            $query = "
                DELETE FROM products
                WHERE id = $productId
            ";
            DB::update($query);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "DeleteProduct | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }
}
