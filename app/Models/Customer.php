<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Customer
{
    const logPath = "customer";

    public function showAll($request): array
    {
        $limit = $request->limit;

        return DB::select("
            SELECT *
            FROM customers
            LIMIT $limit
        ");
    }

    public function showWithId($request): array
    {
        $customerId = $request->id;

        return DB::select("
            SELECT *
            FROM customers
            WHERE id = $customerId
        ");
    }

    public function create($request)
    {
        DB::beginTransaction();

        try {
            $name = $request->name;
            $domicile = $request->domicile;
            $gender = $request->gender;

            $query = "
                INSERT INTO customers (name, domicile, gender)
                VALUES (:name, :domicile, :gender)
            ";
            $params = ['name' => $name, 'domicile' => $domicile, 'gender' => $gender];

            DB::insert($query, $params);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "CreateCustomer | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }

    public function update($request)
    {
        DB::beginTransaction();

        try {
            $customerId = $request->id;
            $dataRequest = $request->all();
            unset($dataRequest['id']);
            $arrayKeysRequest = array_keys($dataRequest);

            if (empty($arrayKeysRequest)) {
                return 0;
            }

            $vCol = array_map(function ($item) {
                return "$item = :$item";
            }, $arrayKeysRequest);
            $vCol = implode(', ', $vCol);

            $query = "
                UPDATE customers
                SET $vCol
                WHERE id = $customerId
            ";
            $params = [];
            foreach ($arrayKeysRequest as $item) {
                $params[$item] = $dataRequest[$item];
            }

            DB::update($query, $params);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "UpdateCustomer | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }

    public function delete($request)
    {
        DB::beginTransaction();

        try {
            $customerId = $request->id;

            $query = "
                DELETE FROM customers
                WHERE id = $customerId
            ";
            DB::update($query);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            d_log('critical', "DeleteCustomer | message: {$exception->getMessage()} | lineCode: {$exception->getLine()}", self::logPath, true);
            responseJSON(1, [], "There's something wrong, Please contact administrator.", 500)->send();
            die();
        }
    }
}
