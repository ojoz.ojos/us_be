<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    private $product;

    public function __construct()
    {
        $this->product = new Product();
    }

    public function showAll(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:limit',
            'limit' => 'required|int'
        ];
        $customAttributes = [
            'limit' => 'Limit'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $data = $this->product->showAll($request);
            return responseJSON(3, $data, "Success Get Products.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function showWithId(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id',
            'id' => 'required|exists:products,id'
        ];
        $customAttributes = [
            'id' => 'Product Id'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $data = $this->product->showWithId($request);
            return responseJSON(3, $data, "Success Get Product.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function create(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:name,category,price',
            'name' => 'required|max:255',
            'category' => 'required|max:255',
            'price' => 'required|numeric|between:0,9999999999.99'
        ];
        $customAttributes = [
            'name' => 'Product Name',
            'category' => 'Category',
            'price' => 'Price'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->product->create($request);
            return responseJSON(3, [], "Success Create Product.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id,name,category,price',
            'id' => 'required|exists:products,id',
            'name' => 'nullable|max:255',
            'category' => 'nullable|max:255',
            'price' => 'nullable|numeric|between:0,9999999999.99'
        ];
        $customAttributes = [
            'id' => 'Product Id',
            'name' => 'Product Name',
            'category' => 'Category',
            'price' => 'Price'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->product->update($request);
            return responseJSON(3, [], "Success Update Product.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function delete(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id',
            'id' => 'required|exists:products,id'
        ];
        $customAttributes = [
            'id' => 'Product Id',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->product->delete($request);
            return responseJSON(3, [], "Success Delete Product.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }
}
