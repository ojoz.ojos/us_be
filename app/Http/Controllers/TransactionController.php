<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    private $transaction;

    public function __construct()
    {
        $this->transaction = new Transaction();
    }

    public function showAll(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:limit',
            'limit' => 'required|int'
        ];
        $customAttributes = [
            'limit' => 'Limit'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $data = $this->transaction->showAll($request);
            return responseJSON(3, $data, "Success Get Transactions.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function showWithId(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id',
            'id' => 'required|exists:transactions,id'
        ];
        $customAttributes = [
            'id' => 'Transaction Id'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $data = $this->transaction->showWithId($request);
            return responseJSON(3, $data, "Success Get Transaction.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function create(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:date,customer,products',
            'date' => 'required|date_format:Y-m-d H:i:s',
            'customer' => 'required|exists:customers,id',
            'products' => 'required'
        ];
        $customAttributes = [
            'date' => 'Date',
            'customer' => 'Customer Id',
            'products' => 'Products'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->transaction->create($request);
            return responseJSON(3, [], "Success Create Transaction.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id,date,customer_id,subtotal',
            'id' => 'required|exists:transactions,id',
            'date' => 'nullable|date_format:Y-m-d H:i:s',
            'customer_id' => 'nullable|exists:customers,id',
            'subtotal' => 'nullable|numeric|between:0,9999999999.99'
        ];
        $customAttributes = [
            'id' => 'Transaction Id',
            'date' => 'Date',
            'customer_id' => 'Customer Id',
            'subtotal' => 'Subtotal'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->transaction->update($request);
            return responseJSON(3, [], "Success Update Transaction.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function delete(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id',
            'id' => 'required|exists:orders,id'
        ];
        $customAttributes = [
            'id' => 'Transaction Id',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->transaction->delete($request);
            return responseJSON(3, [], "Success Delete Transaction.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }
}
