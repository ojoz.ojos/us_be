<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    private $customer;

    public function __construct()
    {
        $this->customer = new Customer();
    }

    public function showAll(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:limit',
            'limit' => 'required|int'
        ];
        $customAttributes = [
            'limit' => 'Limit'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $data = $this->customer->showAll($request);
            return responseJSON(3, $data, "Success Get Customers.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function showWithId(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id',
            'id' => 'required|exists:customers,id'
        ];
        $customAttributes = [
            'id' => 'Customer Id'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $data = $this->customer->showWithId($request);
            return responseJSON(3, $data, "Success Get Customer.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function create(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:name,domicile,gender',
            'name' => 'required|max:100',
            'domicile' => 'required|max:100',
            'gender' => 'required|in:PRIA,WANITA'
        ];
        $customAttributes = [
            'name' => 'Customer Name',
            'domicile' => 'Domicile',
            'gender' => 'Gender'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->customer->create($request);
            return responseJSON(3, [], "Success Create Customer.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id,name,domicile,gender',
            'id' => 'required|exists:customers,id',
            'name' => 'nullable|max:100',
            'domicile' => 'nullable|max:100',
            'gender' => 'nullable|in:PRIA,WANITA'
        ];
        $customAttributes = [
            'id' => 'Customer Id',
            'name' => 'Customer Name',
            'domicile' => 'Domicile',
            'gender' => 'Gender'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->customer->update($request);
            return responseJSON(3, [], "Success Update Customer.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function delete(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id',
            'id' => 'required|exists:customers,id'
        ];
        $customAttributes = [
            'id' => 'Customer Id',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->customer->delete($request);
            return responseJSON(3, [], "Success Delete Customer.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }
}
