<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    private $order;

    public function __construct()
    {
        $this->order = new Order();
    }

    public function showAll(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:limit',
            'limit' => 'required|int'
        ];
        $customAttributes = [
            'limit' => 'Limit'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $data = $this->order->showAll($request);
            return responseJSON(3, $data, "Success Get Orders.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function showWithId(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id',
            'id' => 'required|exists:orders,id'
        ];
        $customAttributes = [
            'id' => 'Order Id'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $data = $this->order->showWithId($request);
            return responseJSON(3, $data, "Success Get Order.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function create(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:date,customer_id,subtotal',
            'date' => 'required|date_format:Y-m-d H:i:s',
            'customer_id' => 'required|exists:customers,id',
            'subtotal' => 'required|numeric|between:0,9999999999.99'
        ];
        $customAttributes = [
            'date' => 'Date',
            'customer_id' => 'Customer Id',
            'subtotal' => 'Subtotal'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->order->create($request);
            return responseJSON(3, [], "Success Create Order.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id,date,customer_id,subtotal',
            'id' => 'required|exists:orders,id',
            'date' => 'nullable|date_format:Y-m-d H:i:s',
            'customer_id' => 'nullable|exists:customers,id',
            'subtotal' => 'nullable|numeric|between:0,9999999999.99'
        ];
        $customAttributes = [
            'id' => 'Order Id',
            'date' => 'Date',
            'customer_id' => 'Customer Id',
            'subtotal' => 'Subtotal'
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->order->update($request);
            return responseJSON(3, [], "Success Update Order.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }

    public function delete(Request $request)
    {
        $rules = [
            '*' => 'allowed_attributes:id',
            'id' => 'required|exists:orders,id'
        ];
        $customAttributes = [
            'id' => 'Order Id',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($customAttributes);

        if (!$validator->fails()) {
            $this->order->delete($request);
            return responseJSON(3, [], "Success Delete Order.");
        } else {
            return responseJSON(1, [], $validator->errors(), 400);
        }
    }
}
