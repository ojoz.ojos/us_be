<?php

// To get response message as global function
function responseJSON($key, $data, $message, $code = 200)
{
    ($key === 3) ? $request = 'success' :
        (($key === 2) ? $request = 'warning' :
            $request = 'error');

    $data = collect((object) $data);

    $response = [
        'total' => ($data == null ? 0 : count($data)),
        'data'   => $data,
        'status' => (object) [
            'code'    => $key,
            'responseText' => $request,
            'message' => $message,
            'httpStatus' => $code
        ]
    ];

    return response()->json($response, $code);
}
