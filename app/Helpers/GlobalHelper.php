<?php

use Illuminate\Support\Facades\Log;

if (!function_exists('d_log')) {
    function d_log($level, $message, $path = 'laravel', bool $daily = false)
    {
        $levels = ['emergency', 'alert', 'critical', 'error', 'warning', 'notice', 'info', 'debug'];

        if (!in_array($level, $levels)) {
            $level = 'debug';
        }

        $message = print_r($message, true);

        Log::build([
            'driver' => 'single',
            'path' => storage_path("logs/$path.log"),
        ])->$level($message);

        if ($daily) {
            Log::channel('daily')->$level($message);
        }
    }
}
